"""Тестирование хендлеров сервиса."""

import json
import re

import pytest
from starlette.status import HTTP_200_OK

from src.service import Shorter


@pytest.mark.asyncio
async def test_get_short_link(client, monkeypatch):
    """Эндпоинт /short возвращает ожидаемый ответ."""
    link_data = {
        'long_link': 'http://test_long',
    }

    async def mock_add_link(long_link, *args, **kwargs):
        return 'YkvSfv'

    with monkeypatch.context() as mock:
        mock.setattr(Shorter, 'add_link', mock_add_link)

        response = client.post('/short', content=json.dumps(link_data))

        assert response.status_code == HTTP_200_OK
        assert re.match(r'http://localhost:24020/[0-9a-zA-Z]{6}', response.json()['short_link']), 'Incorrect URL'


@pytest.mark.asyncio
async def test_redirect(client, monkeypatch):
    """Эндпоинт / возвращает статус код 200."""
    async def mock_get_link(short_link, *args, **kwargs):
        return 'http://localhost:24020/docs'

    with monkeypatch.context() as mock:
        mock.setattr(Shorter, 'get_link', mock_get_link)

        response = client.get('/YkvSfv')

    assert response.status_code == HTTP_200_OK, 'Bad request'
    assert response.text is not None, 'Response is empty'
