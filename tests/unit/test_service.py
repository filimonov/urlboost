"""Тесты компонента сокращателя."""

import pytest

from src.db.requests import DatabaseRequests
from src.service import Shorter


@pytest.mark.asyncio
async def test_add_link(db_session):
    """Тест добавления ссылок."""
    database = DatabaseRequests(db_session)
    long_link = 'http://test_long_link'

    shorter = Shorter(database)
    short_hash = await shorter.add_link(long_link)

    assert short_hash is not None, 'Hash should not be empty'
    assert len(short_hash) == 6, 'Incorrect hash'


@pytest.mark.asyncio
async def test_get_link(db_session):
    """Тест получения ссылок."""
    database = DatabaseRequests(db_session)
    long_link = 'http://test_long_link'

    shorter = Shorter(database)
    short_hash = await shorter.add_link(long_link)

    link = await shorter.get_link(short_hash)

    assert link == long_link, 'Incorrect long link'
