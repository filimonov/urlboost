"""Тест технических хендлеров."""

from starlette.status import HTTP_200_OK

from src.db.requests import DatabaseRequests


def test_app_is_up(client):
    """Тест проверки жизнеспособности приложения."""
    response = client.get('/healthz/up')

    assert response.status_code == HTTP_200_OK


def test_app_is_ready(client, monkeypatch):
    """Тест проверки готовности приложения."""
    def mock_readiness():
        return True

    with monkeypatch.context() as mock:
        mock.setattr(DatabaseRequests, 'is_database_ready', mock_readiness)
        response = client.get('/healthz/ready')

    assert response.status_code == HTTP_200_OK
