"""Тест сбора метрик."""

from starlette.status import HTTP_200_OK


def test_metrics(client):
    """Метрики можно получить по эндпоинту /healthz/metrics."""
    client.get('/healthz/up')
    response = client.post('/healthz/metrics')

    assert response.status_code == HTTP_200_OK
    assert response.text is not None, 'Metrics are not found'
