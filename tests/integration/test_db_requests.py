"""Тесты запросов к БД."""

import pytest

from src.db.requests import DatabaseRequests
from src.schemas import NewURLInput


@pytest.mark.asyncio
async def test_is_database_ready(db_session):
    """Проверка готовности БД."""
    database = DatabaseRequests(db_session)

    readiness = await database.is_database_ready()

    assert readiness is True, 'Database is not ready'


@pytest.mark.asyncio
async def test_add_new_link(db_session):
    """Тест добавления нового аккаунта."""
    database = DatabaseRequests(db_session)

    long_link = 'http://test_link'
    short_hash = 'TeSttT'
    link_data = NewURLInput(long_link=long_link, short_hash=short_hash)
    await database.add_new_link(link_data)

    record = await database.get_long_link_by_hash(short_hash)

    assert record.long_link == long_link, 'Wrong link'
    assert record.short_hash == short_hash, 'Wrong hash'


@pytest.mark.asyncio
async def test_get_long_link_by_hash(db_session):
    """Тест добавления нового аккаунта."""
    database = DatabaseRequests(db_session)

    long_link = 'http://test_link_2'
    short_hash = 'TeSssT'
    link_data = NewURLInput(long_link=long_link, short_hash=short_hash)
    await database.add_new_link(link_data)

    record = await database.get_long_link_by_hash(short_hash)

    assert record.long_link == long_link, 'Wrong link'
    assert record.short_hash == short_hash, 'Wrong hash'


@pytest.mark.asyncio
async def test_is_long_link_exists(db_session):
    """Тест получения записи по длинной ссылке."""
    database = DatabaseRequests(db_session)

    long_link = 'http://test_link_3'
    short_hash = 'TeessT'
    link_data = NewURLInput(long_link=long_link, short_hash=short_hash)
    await database.add_new_link(link_data)

    record = await database.is_long_link_exists(long_link)

    assert record.long_link == long_link, 'Wrong link'
    assert record.short_hash == short_hash, 'Wrong hash'
