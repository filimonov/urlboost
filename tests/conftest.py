"""Общее хранилище фикстур pytest."""

import asyncio

import asyncpg
import pytest
import pytest_asyncio
from fastapi.testclient import TestClient
from pydantic import PostgresDsn
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from src.main import app
from src.models.urls import urls
from src.settings import settings

db_user = settings.postgres_user
db_pass = settings.postgres_password
db_host = settings.postgres_host
db_port = settings.postgres_port
db_root = settings.db_root


@pytest.fixture(scope='session')
def client():
    """Запуск тестового клиента."""
    test_client = TestClient(app)
    yield test_client


@pytest.fixture(scope='session')
def event_loop():
    """Создание event loop."""
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope='session')
async def flush_db():
    """Создание тестовой БД."""
    database_url = str(PostgresDsn.build(
        scheme='postgresql',
        username=db_user,
        password=db_pass,
        host=db_host,
        port=db_port,
        path=db_root,
    ))

    connection = await asyncpg.connect(database_url)
    await connection.execute('DROP DATABASE IF EXISTS afil_test_db')
    await connection.execute('CREATE DATABASE afil_test_db')
    await connection.close()


@pytest_asyncio.fixture(scope='session')
async def db_engine(flush_db):
    """Создание асинхронного движка."""
    database_url = str(PostgresDsn.build(
        scheme='postgresql+asyncpg',
        username=db_user,
        password=db_pass,
        host=db_host,
        port=db_port,
        path='afil_test_db',
    ))
    yield create_async_engine(database_url, echo=True)


@pytest_asyncio.fixture(scope='session')
async def new_db_schema(db_engine):
    """Применение миграций."""
    async with db_engine.begin() as connection:
        await connection.run_sync(urls.metadata.create_all)
    yield
    await db_engine.dispose()


@pytest_asyncio.fixture(scope='session')
async def db_session(new_db_schema, db_engine):
    """Создание тестовой сессии."""
    pg_session = async_sessionmaker(db_engine, expire_on_commit=False)
    yield pg_session
