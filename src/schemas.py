"""Модели Pydantic."""

from pydantic import BaseModel


class LongLinkInput(BaseModel):
    """Модель длинной ссылки."""

    long_link: str


class NewURLInput(BaseModel):
    """Модель длинной ссылки."""

    long_link: str
    short_hash: str
