"""API интерфейс."""

from fastapi import APIRouter
from starlette.responses import JSONResponse, RedirectResponse
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND

from src.db.controller import async_session
from src.db.requests import DatabaseRequests
from src.schemas import LongLinkInput
from src.service import Shorter

link = APIRouter()
database = DatabaseRequests(async_session)
shorter = Shorter(database)


@link.post('/short')
async def get_short_link(link_data: LongLinkInput):
    """Получение короткой ссылки из длинной."""
    short_hash = await shorter.add_link(link_data.long_link)
    short_link = f'http://localhost:24020/{short_hash}'
    return JSONResponse(
        content={'short_link': short_link},
        status_code=HTTP_200_OK,
    )


@link.get('/{short_link}')
async def redirect(short_link: str):
    """Перенаправление по короткой ссылке."""
    long_link = await shorter.get_link(short_link)
    if long_link:
        return RedirectResponse(url=long_link)
    return JSONResponse(
        content={'error': 'Unknown short link'},
        status_code=HTTP_404_NOT_FOUND,
    )
