"""Модуль с моделью БД для ссылок."""
from sqlalchemy import BigInteger, Column, MetaData, String, Table

metadata = MetaData()
link_max_length = 200

urls = Table(
    'urls',
    metadata,
    Column('id', BigInteger, primary_key=True),
    Column('long_link', String(link_max_length), nullable=False),
    Column('short_hash', String(10), nullable=False),
)
