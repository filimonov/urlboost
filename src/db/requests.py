"""Модуль с запросами к БД."""

import asyncpg
from pydantic import PostgresDsn
from sqlalchemy.ext.asyncio import AsyncSession

from src.models.urls import urls
from src.schemas import NewURLInput
from src.settings import settings


class DatabaseRequests(object):
    """Запросы к БД."""

    def __init__(self, async_session):
        self.async_session: AsyncSession = async_session

    async def is_database_ready(self):
        """Проверка готовности БД."""
        db_uri = str(PostgresDsn.build(
            scheme='postgresql',
            username=settings.postgres_user,
            password=settings.postgres_password,
            host=settings.postgres_host,
            port=settings.postgres_port,
            path=settings.db_root,
        ))

        connection = await asyncpg.connect(db_uri)
        response = await connection.execute('SELECT 1')
        if response:
            return True

    async def add_new_link(
        self,
        link_data: NewURLInput,
    ):
        """Добавить новые ссылки."""
        query = urls.insert().values(**link_data.model_dump())

        async with self.async_session() as conn:
            response = await conn.execute(query)
            if response is None:
                raise ValueError('Link was not added')
            await conn.commit()

    async def get_long_link_by_hash(
        self,
        short_hash: str,
    ) -> str:
        """Получить ссылку по хешу."""
        query = urls.select().where(urls.c.short_hash == short_hash)

        async with self.async_session() as conn:
            user_data = await conn.execute(query)
            if user_data.rowcount == 0:
                return False
            return user_data.mappings().one_or_none()

    async def is_long_link_exists(
        self,
        long_link: str,
    ) -> str:
        """Получить запись по длинной ссылке."""
        query = urls.select().where(urls.c.long_link == long_link)

        async with self.async_session() as conn:
            user_data = await conn.execute(query)
            if user_data.rowcount == 0:
                return False
            return user_data.mappings().one_or_none()
