"""Настройка соединения с БД."""

import asyncpg
from pydantic import PostgresDsn
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from src.settings import settings

db_user = settings.postgres_user
db_pass = settings.postgres_password
db_host = settings.postgres_host
db_port = settings.postgres_port
db_name = settings.postgres_db
db_root = settings.db_root

DATABASE_URI = str(PostgresDsn.build(
    scheme='postgresql+asyncpg',
    username=db_user,
    password=db_pass,
    host=db_host,
    port=db_port,
    path=db_name,
))

async_engine = create_async_engine(DATABASE_URI)

async_session = async_sessionmaker(
    async_engine,
    expire_on_commit=False,
)


async def create_db():
    """Создает базу данных при первом запуске."""
    db_uri = str(PostgresDsn.build(
        scheme='postgresql',
        username=db_user,
        password=db_pass,
        host=db_host,
        port=db_port,
        path=db_root,
    ))

    connection = await asyncpg.connect(db_uri)
    databases = await connection.fetch('SELECT datname FROM pg_database')
    if ('url_db',) not in databases:
        await connection.execute('CREATE DATABASE url_db')
    await connection.close()
