"""Бизнес-логика сервиса."""

import shortuuid

from src.db.requests import DatabaseRequests
from src.schemas import NewURLInput


class Shorter(object):
    """Компонент сокращателя."""

    def __init__(self, database):
        self.database: DatabaseRequests = database

    async def add_link(self, long_link) -> int:
        """Добавить новую пару ссылок."""
        link_data = await self.database.is_long_link_exists(long_link)
        if link_data:
            return link_data.short_hash
        short_hash = self._generate_hash()
        while await self.database.get_long_link_by_hash(short_hash):
            short_hash = self._generate_hash()
        new_data = NewURLInput(
            long_link=long_link,
            short_hash=short_hash,
        )
        await self.database.add_new_link(new_data)
        return short_hash

    async def get_link(self, short_link) -> str:
        """Получить длинную ссылку по короткой."""
        link_data = await self.database.get_long_link_by_hash(short_link)
        if link_data:
            return link_data.long_link
        return False

    def _generate_hash(self) -> str:
        """Сгенерировать уникальный хеш."""
        return shortuuid.random()[:6]
