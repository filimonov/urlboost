"""Файл для настроек pydantic."""

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """Управление переменными окружения."""

    app_host: str
    app_port: int
    postgres_host: str
    postgres_port: int
    postgres_user: str
    postgres_password: str
    postgres_db: str
    db_root: str
    jaeger_host: str


settings = Settings()
