"""Конфигурация проекта."""

from jaeger_client import Config

from src.settings import settings

jaeger_config = Config(
    config={
        'sampler': {
            'type': 'const',
            'param': 1,
        },
        'local_agent': {
            'reporting_host': settings.jaeger_host,
            'reporting_port': 6831,
        },
        'logging': True,
    },
    service_name='url_service',
    validate=True,
)
