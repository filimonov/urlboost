"""Middleware сервиса."""

from fastapi import Request, Response
from opentracing import (
    InvalidCarrierException,
    SpanContextCorruptedException,
    global_tracer,
    propagation,
    tags,
)
from prometheus_client import CollectorRegistry, Counter

prometheus_registry = CollectorRegistry()

request_counter = Counter(
    'afil_url_http_request',
    'Подсчет http запросов.',
    ['method', 'endpoint', 'status_code'],
    registry=prometheus_registry,
)


async def metrics_middleware(request: Request, call_next):
    """Middleware для реализации логгирования запросов."""
    response: Response = await call_next(request)

    request_counter.labels(
        method=request.method,
        endpoint=request.url.path,
        status_code=str(response.status_code),
    ).inc()
    return response


async def tracing_middleware(request: Request, call_next):
    """Middleware для реализации трейсинга."""
    path = request.url.path
    if path.startswith('/healthz'):
        return await call_next(request)
    try:
        span_ctx = global_tracer().extract(
            propagation.Format.HTTP_HEADERS,
            request.headers,
        )
    except (InvalidCarrierException, SpanContextCorruptedException):
        span_ctx = None
    span_tags = {
        tags.SPAN_KIND: tags.SPAN_KIND_RPC_SERVER,
        tags.HTTP_METHOD: request.method,
        tags.HTTP_URL: request.url,
    }
    with global_tracer().start_active_span(
        str(request.url.path), child_of=span_ctx, tags=span_tags,
    ):
        return await call_next(request)
