"""Контроллер fastapi."""

import asyncio
from contextlib import asynccontextmanager

import uvicorn
from fastapi import Depends, FastAPI, responses, status
from prometheus_client import make_asgi_app
from starlette.middleware.base import BaseHTTPMiddleware

from src.config import jaeger_config
from src.db.controller import async_engine, async_session, create_db
from src.db.requests import DatabaseRequests
from src.middleware import (
    metrics_middleware,
    prometheus_registry,
    tracing_middleware,
)
from src.router import link
from src.settings import settings

database = DatabaseRequests(async_session)


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Lifespan приложения."""
    tracer = jaeger_config.initialize_tracer()
    yield {'jaeger_tracer': tracer}
    await async_engine.dispose()


app = FastAPI(lifespan=lifespan)


@app.get('/healthz/up')
async def live():
    """LivenessProbe."""
    return responses.Response(status_code=status.HTTP_200_OK)


@app.get('/healthz/ready')
async def ready(
    is_ready: bool = Depends(database.is_database_ready),  # noqa: WPS404, B008
):
    """ReadinessProbe."""
    if is_ready:
        return responses.Response(
            status_code=status.HTTP_200_OK,
        )
    return responses.Response(
        status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
    )


app.include_router(link, tags=['link'])

metrics_app = make_asgi_app(registry=prometheus_registry)
app.mount('/healthz/metrics', metrics_app)
app.add_middleware(BaseHTTPMiddleware, dispatch=metrics_middleware)
app.add_middleware(BaseHTTPMiddleware, dispatch=tracing_middleware)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_db())
    uvicorn.run(
        app=app,
        host=settings.app_host,
        port=settings.app_port,
    )
